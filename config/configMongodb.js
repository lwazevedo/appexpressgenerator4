var MongoClient = require('mongodb').MongoClient;

MongoClient.connect('mongodb://172.17.0.2:27017/admin', function(err, db) {
  if (err) {
    throw err;
  }
  db.collection('system.users').find().toArray(function(err, result) {
    if (err) {
      throw err;
    }
    console.log(result);
  });
});
